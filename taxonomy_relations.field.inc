<?php

/**
 * @file
 * Creating field for the taxonomy relations
 */


/**
 * Implements hook_field_info().
 */
function taxonomy_relations_field_info() {
	return array(
    'taxonomy_relations' => array(
      'label' => t('Taxonomy relations'),
      'description' => t('Taxonomy Relations field'),
      'default_widget' => 'taxonomy_relations',
      'default_formatter' => 'taxonomy_relations_unformatted_list',
      'settings' => array(
        'parent' => array('vid' => 0),
        'child' => array('vid' => 0),
	),
	   'property_type' => 'taxonomy_relations',
     'property_callbacks' => array('taxonomy_relations_property_info_callback'),
	),
	);
}


/**
 * Implements hook_field_widget_info().
 */
function taxonomy_relations_field_widget_info() {
	$settings = array(
    'general' => array(
      'required' => TRUE,
      'prefix' => '',
      'suffix' => '',
	),
   'select' => array(
      'allowed_values' => array(),
	),
	);


	return array(
    'taxonomy_relations' => array(
      'label' => t('Taxonomy relations'),
      'field types' => array('taxonomy_relations'),      
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
	),
		'settings' => array(
       'parent' => array(
	        'general' => $settings['general'],
          'select' => $settings['select'],
	),
     'child' => array(
	      'general' => $settings['general'],
        'select' => $settings['select'],

	),
	),
	),
	);
}

/**
 * Callback to alter the property info of taxonomy relations field
 *
 * @see addressfield_field_info().
 */
function taxonomy_relations_property_info_callback(&$info, $entity_type, $field, $instance, $field_type) {
	$property = &$info[$entity_type]['bundles'][$instance['bundle']]['properties'][$field['field_name']];

	$property['getter callback'] = 'entity_metadata_field_verbatim_get';
	$property['setter callback'] = 'entity_metadata_field_verbatim_set';
	foreach (array('parent', 'child') as $subfield) {
		$property['property info'][$subfield] = array(
      'type' => 'text',
      'label' => t(ucfirst($subfield) . ' subfield'),
      'setter callback' => 'entity_property_verbatim_set',
		);
	}
	unset($property['query callback']);
}

/**
 * Implements hook_field_settings_form().
 */
function taxonomy_relations_field_settings_form($field, $instance, $has_data) {
	$settings = $field['settings'];

	$parent_vocabulary = array();
	$child_vocabulary = array();

	//Getting the list of vocabularies parent & child
	$vocabularies = db_select('taxonomy_relations_voc', 'rv')->fields('rv', array('parent_vid', 'child_vid'))->condition('rv.parent_vid', 0 , '>')->execute();

	if ($vocabularies->rowCount() > 0) {
		$vocabularies_object = $vocabularies->fetchAll();

		//Iterating through vocabularies
		foreach ($vocabularies_object as $vocabularies_option) {
			//Getting the parent vocabulary
			$voc_parent = taxonomy_vocabulary_load($vocabularies_option->parent_vid);
			$parent_vocabulary[$voc_parent->vid] = $voc_parent->name;
			//Getting the child vocabulary
			$voc_child = taxonomy_vocabulary_load($vocabularies_option->child_vid);
			$child_vocabulary[$voc_child->vid] = $voc_child->name;
		}
	}

	$form['parent'] = array(
      '#type' => 'select',
      '#title' => t("Parent Taxonomy"),
      '#default_value' => '',
      '#required' => TRUE,
      '#description' => t("Parent Taxonomy."),
	    '#options' => $parent_vocabulary,
      //'#element_validate' => array('element_validate_integer_positive'),
      '#disabled' => $has_data,
	);

	$form['child'] = array(
      '#type' => 'select',
      '#title' => t("Child Taxonomy"),
      '#default_value' => '',
      '#required' => TRUE,
      '#description' => t("Child Taxonomy."),
      '#options' => $child_vocabulary,
      ///'#element_validate' => array('element_validate_integer_positive'),
      '#disabled' => $has_data,
	);

	return $form;

}

/**
 * Implements hook_field_widget_form().
 */

function taxonomy_relations_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

	// Add display_field setting to field because file_field_widget_form() assumes it is set.
	$field['settings']['display_field'] = 0;

	$settings = $instance['settings'];

	//Getting the parent & child vocabulary from field settings
	$parent_vid = $field['settings']['parent'];
	$child_vid = $field['settings']['child'];

	$parent_term_options[] = t('Select');
	$child_term_options[] = t('Select');

	//checking if parent vocabulary is selected
	if (isset($parent_vid)) {
		$parent_tree = taxonomy_get_tree($parent_vid);

		if (!empty($parent_tree)) {
			foreach ($parent_tree as $parent_term) {
				$parent_term_options[$parent_term->tid] = $parent_term->name;
			}
		}
	}

	//Checking if child vocabulary is selected.
	if (isset($child_vid)) {
		$child_tree = taxonomy_get_tree($child_vid);

		if (!empty($child_tree)) {
			foreach ($child_tree as $child_term) {
				$child_term_options[$child_term->tid] = $child_term->name;
			}
		}
	}


	$elements = array(
    '#theme_wrappers' => array('form_element'),
	);

	$elements[$delta]['parent'] = array(
		'#type' => 'select',
		'#title' => t('Parent Term 1'),
		'#description' => t('Select parent term'),
		'#options' => $parent_term_options,
	  ///'#element_validate' => array('taxonomy_relations_parent_element_validate'),
	 '#default_value' => (isset($items[$delta]['parent']) ? $items[$delta]['parent'] : ''),	
	);

	$elements[$delta]['child'] = array(
		'#type' => 'select',
		'#title' => t('Child Term 1'),
		'#description' => t('Select chlid term'),
		'#options' => $child_term_options,
	  ///'#element_validate' => array('taxonomy_relations_parent_element_validate'),
	  '#default_value' => (isset($items[$delta]['child']) ? $items[$delta]['child'] : ''),
	);

	return $element + $elements;
}


/**
 * Implements hook_field_formatter_info().
 */
function taxonomy_relations_field_formatter_info() {
	foreach (array('parent', 'child') as $subfield) {
		$settings[$subfield] = array(
      'hidden' => 0,
      'format' => '_none',
      'prefix' => '',
      'suffix' => '',
		);
	}
	$settings['parent']['suffix'] = ':&nbsp;';

	return array(
    'taxonomy_relations_unformatted_list' => array(
      'label' => t('Unformatted list'),
      'field types' => array('taxonomy_relations'),
      'settings' => $settings + array('style' => 'inline'),
	),
	);
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function taxonomy_relations_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {

	$settings = $instance['display'][$view_mode]['settings'];
	$element = array('#tree' => TRUE);

	$style_settings = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#options' => array(
      'inline' => t('Inline'),
      'block' => t('Block'),
      'link' => t('Link'),
	),
    '#default_value' => $settings['style'],
	);

}


/**
 * Implements hook_field_is_empty().
 */
function taxonomy_relations_field_is_empty($item, $field) {
	return $item['parent'] = '' && $item['child'] = '';
}



/**
 * Implements hook_field_validate().
 */
function taxonomy_relations_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
	$settings = $instance['widget']['settings'];

	$error = array(
      'error' => 'taxonomy_relations_required',
      'error_element' => array('parent' => TRUE, 'child' => TRUE),
      'message' => t('%name field is required.', array('%name' => $instance['label'])),
	);

	$field_is_empty = TRUE;

	if ($entity_type) {
		foreach ($items as $item) {
			if ($item['parent'] !== '' || $item['child'] !== '') {
				$field_is_empty = FALSE;
				$error['error_element']['parent'] = $settings['parent']['general']['required'] && $item['parent'] === '';
				$error['error_element']['child'] = $settings['child']['general']['required'] && $item['child'] === '';
				if ($error['error_element']['parent'] || $error['error_element']['child']) {
					$errors[$field['field_name']][$langcode][$delta][] = $error;
				}
			}
		}
	}
}
